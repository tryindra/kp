@extends('layouts.index')

@section('content')

<div class="content-header">
    <div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Dashboard - {{ $bulan_sekarang }}</h1>
			</div>
		</div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
		<div class="row">
			<div class="col-lg-3 col-6">
				<div class="small-box bg-info">
					<div class="inner">
						<h3>{{ $jumlah_barang_masuk }}</h3>
						<p>Jumlah Barang Masuk</p>
					</div>
					<div class="icon">
						<i class="fas fa-dolly-flatbed"></i>
					</div>
					<a href="{{ url('barang_masuk/') }}" class="small-box-footer">
						Selengkapnya <i class="fas fa-arrow-circle-right"></i>
					</a>
				</div>
			</div>
		</div>
		<div class="row">
			@php
			$icon = ['<i class="fas fa-warehouse"></i>', '<i class="fas fa-house-damage"></i>', '<i class="fas fa-wrench"></i>', '<i class="fas fa-people-carry"></i>'];
			$warna = ['bg-success', 'bg-danger', 'bg-warning', 'bg-info'];
			@endphp
			@foreach ($jumlah as $key=>$item)
				
			<div class="col-lg-3 col-6">
				<div class="small-box {{ $warna[$key] }}">
					<div class="inner">
						<h3>{{ $item['jumlah'] }}</h3>
						<p>{{ $item['status'] }}</p>
					</div>
					<div class="icon">
						{!! $icon[$key] !!}
					</div>
					<a href="{{ url('pergerakan_barang/') }}" class="small-box-footer">
						Selengkapnya <i class="fas fa-arrow-circle-right"></i>
					</a>
				</div>
			</div>
			
			@endforeach
		</div>
    </div>
  </section>

@endsection