@extends('layouts.index')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">MASTER BARANG</h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
		<div class="row mb-2">
		</div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-primary card-outline">
					<div class="card-body">
						<form method="POST" action="{{url('barang_masuk/update')}}" autocomplete="off">

						@foreach($data as $datas)

							<input type="hidden" name="id" value="{{ $datas->id }}">
							
							@if ($errors->any())
								<div class="alert alert-danger">
									<strong>PERHATIAN!</strong> Terdapat Error! Silahkan Periksa Kembali Inputan Anda!.
									<br/>
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif

							<!-- CSRF Token -->
							@csrf
							<div class="row justify-content-md-center">
								<div class="col-md-6">
									<div class="form-group @error('nama_barang') has-error @enderror">
										<label for="nama_barang">Nama Barang:</label>

										<select id="nama_barang" name="nama_barang" class="form-control">
											<option value="" disabled>-- PILIH BARANG --</option>
											<option selected>{{ $datas->nama_barang }}</option>
										</select>

										@error('nama_barang')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>
							</div>
							
							<div class="row justify-content-md-center">
								<div class="col-md-6">
									<div class="form-group @error('alamat') has-error @enderror">
										<label for="alamat">Lokasi Penyimpanan:</label>

										<select id="alamat" name="alamat" class="form-control">
											<option value="" disabled>-- PILIH LOKASI PENYIMPANAN --</option>
											<option selected >{{ $datas->nama_lokasi }}</option>
										</select>

										@error('alamat')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>
							</div>
							
							<div class="row justify-content-md-center">
								<div class="col-md-3">
									<div class="form-group @error('tanggal') has-error @enderror">
										<label for="tanggal">Tanggal Barang Masuk:</label>
										
										<div class="input-group date" id="reservationdate" data-target-input="nearest">
											<input type="text" class="form-control" name="tanggal" value="{{ date('d-m-Y H:i', strtotime($datas->tanggal)) }}" readonly disabled/>
										</div>
										@error('tanggal')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group @error('jumlah') has-error @enderror">
										<label for="jumlah">Jumlah:</label>
										<input type="text" class="form-control angka" id="jumlah" placeholder="Masukkan Jumlah" value="{{ $datas->jumlah }}" readonly disabled/>
										
										@error('jumlah')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>
							</div>
							
							<div class="row justify-content-md-center">
								<div class="col-md-6">
									<div class="form-group @error('deskripsi') has-error @enderror">
										<label for="deskripsi">Keterangan/Deskripsi:</label>
										<textarea id="deskripsi" name="deskripsi" class="form-control" placeholder="Masukkan Deskripsi" style="resize: none; height: 100px;">{{ old('deskripsi') }}{{ $datas->deskripsi }}</textarea>

										@error('deskripsi')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>
							</div>

						@endforeach
							
							<br><br>
							<div class="row justify-content-md-center">
								<div class="col-md-6">
									<div class="form-group">
										<button class="btn btn-success">Submit</button>
									</div>
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>
  <!-- /.content -->
@endsection
