$(document).ready( function () 
{
    $('[data-toggle="tooltip"]').tooltip();

    $('textarea').attr('spellcheck', false)

	$('#datepicker').daterangepicker(
	{
        singleDatePicker: true,
        timePicker : true,
        timePicker24Hour  : true,
        locale: { format: 'DD-MM-YYYY HH:mm' }
    });
    
    $('.angka').keyup(function(e)
    {
        if (/\D/g.test(this.value))
        {
            this.value = this.value.replace(/\D/g, '');
        }
    });
    
    $('#dataTablesMasterKategori').DataTable(
    {
        "columnDefs": [ 
        {
            "targets": [3, 4],
            "orderable": false
        }],
        // "ajax": 
        // {
        //     "url": APP_URL+'/load_datatable_kategori',
            
        // }
    });

    $('#dataTablesMasterBarang').DataTable(
    {
        "columnDefs": [ 
        {
            "targets": [4, 5],
            "orderable": false
        }],
    });

    $('#dataTablesMasterLokasi').DataTable(
    {
        "columnDefs": [ 
        {
            "targets": [5, 6],
            "orderable": false
        }],
    });

    $('#dataTablesBarangMasuk').DataTable(
    {
        "columnDefs": [ 
        {
            "targets": [5, 6],
            "orderable": false
        }],
    });

    $('#dataTablesPergerakan').DataTable(
    {
        "columnDefs": [ 
        {
            "targets": [10],
            "orderable": false
        }],
    });
});

function qrcode(id)
{     
    swal(
    {
        text: "L O A D I N G . . . ",
        icon: "info",
        buttons: false,
        allowOutsideClick: false
    });

    setTimeout(() => 
    {
        $.ajax({
            type:'get',
            dataType:'json',
            url: APP_URL+'/pergerakan/qrcode/'+id,
        }).done(function(data)  
        {
            console.log(data);

            swal.close();

            setTimeout(() => 
            {
                if(data.status)
                {
                    var span = document.createElement("span");
                    span.innerHTML = data.qrcode+'<br><br>Klik <b>Print</b> Untuk Mencetak QRCode';

                    swal( 
                    {
                        title: "QRCode",
                        content: span,
                        buttons: ["Tutup", "Print"]
                    })
                    .then((value) => 
                    {
                        if (value) 
                        {     
                            window.open(APP_URL+'/pergerakan/cetak_qrcode/'+id, "_blank"); 
                        }
                    });
                }
                else
                {
                    swal( 
                    {
                        title: "PERHATIAN!",
                        text: data.pesan,
                        icon: "warning",
                        dangerMode: true,
                    });
                }
            }, 100);
        })
        .error(function()  
        {
            swal( 
            {
                title: "PERHATIAN!",
                text: "Terjadi Kesalahan Teknis! Silahkan Coba Lagi.",
                icon: "warning",
                dangerMode: true,
            });
        }); 
    }, 500);
}