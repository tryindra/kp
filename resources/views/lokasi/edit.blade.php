@extends('layouts.index')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">MASTER BARANG</h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
		<div class="row mb-2">
		</div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-primary card-outline">
					<div class="card-body">
						<form method="POST" action="{{url('lokasi/update')}}" autocomplete="off">

						@foreach($data as $datas)

							<input type="hidden" name="id" value="{{ $datas->id }}">
							
							@if ($errors->any())
								<div class="alert alert-danger">
									<strong>PERHATIAN!</strong> Terdapat Error! Silahkan Periksa Kembali Inputan Anda!.
									<br/>
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif

							<!-- CSRF Token -->
							@csrf
							<div class="row justify-content-md-center">
								<div class="col-md-6">
									<div class="form-group @error('nama_lokasi') has-error @enderror">
										<label for="nama_lokasi">Nama Lokasi:</label>
										<input type="text" id="nama_lokasi" name="nama_lokasi" class="form-control" placeholder="Masukkan Nama Lokasi" value="{{ $datas->nama_lokasi }}">

										@error('nama_lokasi')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>
							</div>
							
							<div class="row justify-content-md-center">
								<div class="col-md-6">
									<div class="form-group @error('alamat') has-error @enderror">
										<label for="alamat">Alamat Lokasi:</label>
										<textarea id="alamat" name="alamat" class="form-control" placeholder="Masukkan Alamat" style="resize: none; height: 100px;">{{ $datas->alamat_lokasi }}</textarea>

										@error('alamat')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>
							</div>
							
							<div class="row justify-content-md-center">
								<div class="col-md-6">
									<div class="form-group @error('deskripsi') has-error @enderror">
										<label for="deskripsi">Keterangan/Deskripsi:</label>
										<textarea id="deskripsi" name="deskripsi" class="form-control" placeholder="Masukkan Deskripsi" style="resize: none; height: 100px;">{{ $datas->deskripsi }}</textarea>

										@error('deskripsi')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>
							</div>

							<div class="row justify-content-md-center">
								<div class="col-md-6">
									<div class="form-group @error('status') has-error @enderror">
										<label for="status">Status:</label>
										<div class="custom-control custom-checkbox">
											<input class="custom-control-input" type="checkbox" name="status" id="customCheckbox1" {{ $datas->status ? 'checked' : '' }} value="aktif">
											<label for="customCheckbox1" class="custom-control-label">Aktif</label>
										</div>		
										@error('status')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>
							</div>
							
							<div class="row justify-content-md-center">
								<div class="col-md-6">
									<div class="form-group">
										<button class="btn btn-success">Submit</button>
									</div>
								</div>
							</div>

						@endforeach

					</form>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>
  <!-- /.content -->
@endsection
