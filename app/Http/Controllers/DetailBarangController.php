<?php

namespace App\Http\Controllers;

use App\Pergerakan;
use App\Barang;
use App\Lokasi;
use App\Status;
use App\HistoryBarang;
use DB;

class DetailBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index($id)
    {        
        //
        $ret = DB::table('pergerakan_barang')
        ->select(   "pergerakan_barang.id", DB::raw("DATE_FORMAT(pergerakan_barang.tanggal, '%d-%b-%Y %H:%i') as tanggal"),
                    "barang.nama_barang", "barang_masuk.jumlah as jumlah_barang_masuk", "lokasi.nama_lokasi", "pergerakan_barang.lokasi_id", "pergerakan_barang.catatan",
                    DB::raw("DATE_FORMAT(barang_masuk.tanggal, '%d-%b-%Y %H:%i') as tanggal_barang_masuk"), "pergerakan_barang.status", "status.status as nama_status")
        ->join('barang_masuk', 'barang_masuk.id', '=', 'pergerakan_barang.barang_masuk_id')
        ->join('barang', 'barang.id', '=', 'barang_masuk.barang_id')
        ->join('lokasi', 'lokasi.id', '=', 'pergerakan_barang.lokasi_id')
        ->join('status', 'status.id', '=', 'pergerakan_barang.status')
        ->where('pergerakan_barang.id', $id)
        ->where('pergerakan_barang.hapuskah', 0)
        ->where('barang_masuk.hapuskah', 0)
        ->where('barang.hapuskah', 0)
        ->where('status.hapuskah', 0)
        ->where('lokasi.hapuskah', 0)
        ->get();

        $lokasi_awal = DB::table('pergerakan_barang')
            ->select("lokasi.nama_lokasi")
            ->join('barang_masuk', 'barang_masuk.id', '=', 'pergerakan_barang.barang_masuk_id')
            ->join('lokasi', 'lokasi.id', '=', 'barang_masuk.lokasi_id')
            ->where('pergerakan_barang.id', $id)
            ->where('pergerakan_barang.hapuskah', 0)
            ->where('barang_masuk.hapuskah', 0)
            ->where('lokasi.hapuskah', 0)
            ->get();

        $data_status = Status::where('hapuskah', 0)->orderBy('id', 'asc')->get();
        $data_lokasi = Lokasi::where('hapuskah', 0)->orderBy('nama_lokasi', 'asc')->get();
        $data_history = DB::table('history_barang')
            ->select('history_barang.catatan', 'status.status', 'lokasi.nama_lokasi', DB::raw("DATE_FORMAT(history_barang.tanggal, '%d-%b-%Y %H:%i') as tanggal"))
            ->join('status', 'status.id', '=', 'history_barang.status_akhir')
            ->join('lokasi', 'lokasi.id', '=', 'history_barang.lokasi')
            ->where('history_barang.pergerakan_barang_id', $id)
            ->where('status.hapuskah', 0)
            ->where('lokasi.hapuskah', 0)
            ->orderBy('history_barang.timestamp', 'desc')
            ->get();

        $data = array('data' => $ret, 'data_status' => $data_status, 'data_lokasi' => $data_lokasi, 'lokasi_awal' => $lokasi_awal, 'data_history' => $data_history);

        return view('detailbarang/index')->with($data);
    }
}