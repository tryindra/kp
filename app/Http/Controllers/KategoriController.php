<?php

namespace App\Http\Controllers;

use App\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $ret = Kategori::where('hapuskah', 0)->get();
        $data = array('data' => $ret);
        return view('kategori/index')->with($data);
    }

    public function load_datatable()
    {
        //
        $ret = Kategori::all();
        $data = array('data' => $ret);
        return view('kategori/index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('kategori/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $request->validate(
        [
            'kategori_barang' => 'required|string|max:255|unique:kategori,nama_kategori',
        ],
        [
            'kategori_barang.required' => 'Silahkan Isi Kategori Terlebih Dahulu.',
            'kategori_barang.unique' => 'Kategori Telah Tercatat. Silahkan Masukkan Kategori Lainnya.',
            'kategori_barang.max' => 'Maksimal Terdiri Dari 255 Karakter.',
            'kategori_barang.string' => 'Inputan Harus Berupa Teks Huruf/Angka',
        ]);

        $kategori = new Kategori;
        $kategori->nama_kategori = $request->kategori_barang;
        $kategori->deskripsi = $request->keterangan_barang;
        $kategori->hapuskah = false;
        $kategori->save();

        return redirect()->route('index_kategori');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $ret = Kategori::find($id);

        if($ret)
        {
            $data = array('data' => $ret);
            return view('kategori/edit', ['data' => $data]);
        }
        else
        {
            return redirect()->route('index_kategori');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $validatedData = $request->validate(
        [
            'kategori_barang' => 'required|string|max:255',
            'id' => 'required',
        ],
        [
            'id.required' => 'ID Tidak Ditemukan',
            'kategori_barang.required' => 'Silahkan Isi Kategori Terlebih Dahulu.',
            'kategori_barang.max' => 'Maksimal Terdiri Dari 255 Karakter.',
            'kategori_barang.string' => 'Inputan Harus Berupa Teks Huruf/Angka',
        ]);
        
        $cek = Kategori::where('nama_kategori', $request->kategori_barang)->where('id', '<>', $request->id)->count();
        if($cek)
        {
            return redirect('/kategori/edit/'.$request->id)->withErrors(['kategori_barang' => 'Kategori Telah Tercatat. Silahkan Masukkan Kategori Lainnya.']); 
        }
        else
        {
            $kategori = Kategori::find($request->id);
            $kategori->nama_kategori = $request->kategori_barang;
            $kategori->deskripsi = $request->keterangan_barang;
            $kategori->update();

            return redirect()->route('index_kategori');
        }
    }

    public function hapus(Request $request)
    {
        $validatedData = $request->validate(
        [
            'id' => 'required',
        ],
        [
            'id.required' => 'ID Tidak Ditemukan',
        ]);

        $kategori = Kategori::find($request->id);
        $kategori->hapuskah = true;
        $update = $kategori->update();

        if($update)
        {
            return json_encode(array('status' => true));
        }
        else
        {
            return json_encode(array('status' => false, 'pesan' => 'Data Gagal Dihapus! Silahkan Coba Lagi.'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
