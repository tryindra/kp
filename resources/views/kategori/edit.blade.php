@extends('layouts.index')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">MASTER BARANG</h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
		<div class="row mb-2">
		</div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-primary card-outline">
					<div class="card-body">
						<form method="POST" action="{{url('kategori/update')}}" autocomplete="off">

						@foreach($data as $datas)

							<input type="hidden" name="id" value="{{ $datas->id }}">

						@if ($errors->any())
							<div class="alert alert-danger">
								<strong>PERHATIAN!</strong> Terdapat Error! Silahkan Periksa Kembali Inputan Anda!.
								<br/>
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif

						<!-- CSRF Token -->
						@csrf
						<div class="row justify-content-md-center">
							<div class="col-md-6">
								<div class="form-group @error('kategori_barang') has-error @enderror">
									<label for="kategori_barang">Kategori:</label>
									<input type="text" id="kategori_barang" name="kategori_barang" class="form-control" placeholder="Masukkan Kategori Barang" value="{{ $datas->nama_kategori }}">

									@error('kategori_barang')
									<span class="text-danger">{{ $message }}</span>
									@enderror
								</div>
							</div>
						</div>
						
						<div class="row justify-content-md-center">
							<div class="col-md-6">
								<div class="form-group @error('keterangan_barang') has-error @enderror">
									<label for="keterangan_barang">Keterangan/Deskripsi:</label>
									<textarea id="keterangan_barang" name="keterangan_barang" class="form-control" placeholder="Masukkan Deskripsi" style="resize: none; height: 100px;">{{ $datas->deskripsi }}</textarea>

									@error('keterangan_barang')
									<span class="text-danger">{{ $message }}</span>
									@enderror
								</div>
							</div>
						</div>

						@endforeach

						<br><br>
						
						<div class="row justify-content-md-center">
							<div class="col-md-6">
								<div class="form-group">
									<button class="btn btn-success">Submit</button>
								</div>
							</div>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>
  <!-- /.content -->
@endsection
