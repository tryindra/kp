@extends('layouts.index')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">BARANG MASUK</h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
		<div class="row mb-2">
		</div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-primary card-outline">
					<div class="card-body">
						<form method="POST" action="{{url('lokasi/store')}}" autocomplete="off">

						@if ($errors->any())
							<div class="alert alert-danger">
								<strong>PERHATIAN!</strong> Terdapat Error! Silahkan Periksa Kembali Inputan Anda!.
								<br/>
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif

						<!-- CSRF Token -->
						@csrf
						<div class="row justify-content-md-center">
							<div class="col-md-6">
								<div class="form-group @error('nama_barang') has-error @enderror">
									<label for="nama_barang">Nama Barang:</label>

									<select id="nama_barang" name="nama_barang" class="form-control">
										<option value="" selected disabled>-- PILIH BARANG --</option>

									@foreach ($data_barang as $data_barangs)
										
										<option value="{{ $data_barangs->id }}">{{ $data_barangs->nama_barang }}</option>
									
									@endforeach

									</select>

									@error('nama_barang')
									<span class="text-danger">{{ $message }}</span>
									@enderror
								</div>
							</div>
						</div>
						
						<div class="row justify-content-md-center">
							<div class="col-md-6">
								<div class="form-group @error('alamat') has-error @enderror">
									<label for="alamat">Lokasi Penyimpanan:</label>

									<select id="nama_barang" name="nama_barang" class="form-control">
										<option value="" selected disabled>-- PILIH LOKASI PENYIMPANAN --</option>

									@foreach ($data_lokasi as $data_lokasis)
										
										<option value="{{ $data_lokasis->id }}">{{ $data_lokasis->nama_lokasi }}</option>
									
									@endforeach

									</select>

									@error('alamat')
									<span class="text-danger">{{ $message }}</span>
									@enderror
								</div>
							</div>
						</div>
						
						<div class="row justify-content-md-center">
							<div class="col-md-6">
								<div class="form-group @error('deskripsi') has-error @enderror">
									<label for="deskripsi">Keterangan/Deskripsi:</label>
									<textarea id="deskripsi" name="deskripsi" class="form-control" placeholder="Masukkan Deskripsi" style="resize: none; height: 100px;">{{ old('deskripsi') }}</textarea>

									@error('deskripsi')
									<span class="text-danger">{{ $message }}</span>
									@enderror
								</div>
							</div>
						</div>

						<div class="row justify-content-md-center">
							<div class="col-md-6">
								<div class="form-group @error('status') has-error @enderror">
									<label for="status">Status:</label>
									<div class="custom-control custom-checkbox">
										<input class="custom-control-input" type="checkbox" name="status" id="customCheckbox1" checked value="aktif">
										<label for="customCheckbox1" class="custom-control-label">Aktif</label>
									</div>		
									@error('status')
									<span class="text-danger">{{ $message }}</span>
									@enderror
								</div>
							</div>
						</div>
						
						<div class="row justify-content-md-center">
							<div class="col-md-6">
								<div class="form-group">
									<button class="btn btn-success">Submit</button>
								</div>
							</div>
						</div>

					</form>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>
  <!-- /.content -->
@endsection
