@extends('layouts.index')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">DETAIL PERGERAKAN BARANG</h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
		<div class="row mb-2">
		</div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-primary card-outline">
					<div class="card-header">
						<h4>Data Barang Masuk</h4>
					</div>
					<div class="card-body">

						@foreach($data as $datas)

						<div class="row justify-content-sm-center">
							<div class="col-sm-4 justify-content-sm-end text-right">
								<div>
									<label>Nama Barang:</label>
								</div>
							</div>
							<div class="col-sm-8">
								<div>
									<span>{{ $datas->nama_barang }}</span>
								</div>
							</div>
						</div>
						<div class="row justify-content-sm-center">
							<div class="col-sm-4 justify-content-sm-end text-right">
								<div>
									<label>Tanggal Barang Masuk:</label>
								</div>
							</div>
							<div class="col-sm-8">
								<div>
									<span>{{ $datas->tanggal_barang_masuk }}</span>
								</div>
							</div>
						</div>
						<div class="row justify-content-sm-center">
							<div class="col-sm-4 justify-content-sm-end text-right">
								<div>
									<label>Jumlah Barang Masuk:</label>
								</div>
							</div>
							<div class="col-sm-8">
								<div>
									<span>{{ $datas->jumlah_barang_masuk }}</span>
								</div>
							</div>
						</div>
						<div class="row justify-content-sm-center">
							<div class="col-sm-4 justify-content-sm-end text-right">
								<div>
									<label>Lokasi Awal:</label>
								</div>
							</div>
							<div class="col-sm-8">
								<div>
									<span>{{ $datas->nama_lokasi }}</span>
								</div>
							</div>
						</div>

						@endforeach

					</div>
				</div>
					
				<div class="row">
				@foreach ($status as $datas_status)
					<div class="col-md-6">
						<div class="card card-info card-outline">
							<div class="card-header">
								<h4>{{ $datas_status->status }}</h4>
							</div>
							<div class="card-body">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th width="10px">#</th>
											<th width="100px">Jumlah</th>
											<th>Lokasi</th>
											<th width="10px">#</th>
										</tr>
									</thead>
									<tbody>
									
									@php
										$total = 0;
										$width = 0;
									@endphp
									@foreach ($data_status as $status)
										@if ($status['id_status'] == $datas_status->id)		
											@if (count($status['jumlah']) == 0)
										<tr class="belum_ada_data">
											<td colspan="4">- belum ada data -</td>
										</tr>
											@else
												@foreach ($status['jumlah'] as $data_jumlah)	
													@php
														$total+=$data_jumlah['jumlah'];
													@endphp	
										<tr>						
											<td>{{ ($loop->index + 1) }}</td>
											<td>{{ $data_jumlah['jumlah'] }}</td>
											<td>{{ $data_jumlah['nama_lokasi'] }}</td>
											<td><a href="{{ url('pergerakan/status/') }}/{{$data[0]->id}}/{{$datas_status->id}}/{{$data_jumlah['id_lokasi']}}" class="btn btn-info btn-sm"><i class="fas fa-edit"></i></a></td>
										</tr>
												@endforeach
											@endif
										@endif
									@endforeach	
									
									</tbody>
									<tfoot>
										<td colspan="3"><b>TOTAL</b></td>	
										<td>{{ $total }}</td>	
									</tfoot>

								</table>
							</div>
							<div class="card-footer">
								<div class="row">
									<div class="col-sm-12">
										<div class="progress progress-xs">
											<div class="progress-bar progress-bar bg-info" style="width: {{ ($total / $datas->jumlah_barang_masuk) * 100 }}%"></div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12" style="text-align: center;">
										<span class="badge bg-info">{{ round(($total / $datas->jumlah_barang_masuk) * 100) }}%</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				@endforeach

				</div>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>
  <!-- /.content -->
@endsection
