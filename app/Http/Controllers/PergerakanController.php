<?php

namespace App\Http\Controllers;

use App\Pergerakan;
use App\Barang;
use App\Lokasi;
use App\Status;
use App\HistoryBarang;
use Illuminate\Http\Request;
use DB;
use QrCode;

class PergerakanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $status = Status::where('hapuskah', 0)->orderBy('id', 'asc')->get();
        $array = array();
        
        $data_barang_masuk = DB::table('barang_masuk')
                ->select(   "barang_masuk.id", "barang.nama_barang", "barang_masuk.jumlah", 
                            DB::raw("DATE_FORMAT(barang_masuk.tanggal, '%d-%b-%Y %H:%i') as tanggal"))
                ->join('barang', 'barang.id', '=', 'barang_masuk.barang_id')
                ->where('barang.hapuskah', 0)
                ->where('barang_masuk.hapuskah', 0)
                ->orderBy('barang_masuk.tanggal', 'desc')
                ->get();

        foreach($status as $list_status)
        {
            array_push($array, array("id_status" => $list_status->id, "status" => $list_status->id));
        }
            
        $data_barang = array();
        foreach($data_barang_masuk as $listkey=>$list_barang)
        {
            $jumlah = array();
            $temps = array( "id" => $list_barang->id, "nama_barang" => $list_barang->nama_barang, 
                            "jumlah" => $list_barang->jumlah, "tanggal" => $list_barang->tanggal, "data_pergerakan" => null);
            array_push($data_barang, $temps);

            foreach($array as $key=>$cek_status)
            {
                $id_status = $cek_status['id_status'];
                array_push($jumlah, array("id_status" => $cek_status["id_status"], "jumlah" => null));

                $temp = DB::select
                (   
                    "SELECT
                    ( 
                        SELECT COUNT(id) 
                        FROM pergerakan_barang
                        where barang_masuk_id = '$list_barang->id' and status = '$id_status' and hapuskah = 0
                    ) AS jumlah"
                );

                foreach($temp as $data_temp)
                {
                    $jumlah[$key]['jumlah'] = $data_temp->jumlah;
                }
            }

            $data_barang[$listkey]["data_pergerakan"] = $jumlah;
        }

        // dd($data_barang);

        $data = array('data' => $data_barang, "status" => $status, "data_status" => $status);
        return view('pergerakan/index')->with($data);
    }

    public function load_datatable()
    {
        //
        $ret = Pergerakan::all();
        $data = array('data' => $ret);
        return view('pergerakan/index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data_barang = Barang::where('hapuskah', 0)->where('status', 1)->get();
        $data_lokasi = Lokasi::where('hapuskah', 0)->where('status', 1)->get();
        $data = array('data_barang' => $data_barang, 'data_lokasi' => $data_lokasi);
        
        return view('pergerakan/create')->with($data);
    }

    public function detail($id)
    {
        //
        $array = array();
        $status = Status::where('hapuskah', 0)->orderBy('id', 'asc')->get();
        foreach($status as $list_status)
        {
            array_push($array, array("id_status" => $list_status->id, "status" => $list_status->id));
        }

        $ret = DB::table('pergerakan_barang')
            ->select(   "barang_masuk.id", "barang.nama_barang", "pergerakan_barang.status", "barang_masuk.jumlah as jumlah_barang_masuk", 
                        DB::raw("DATE_FORMAT(pergerakan_barang.tanggal, '%d-%b-%Y %H:%i') as tanggal"), "lokasi.nama_lokasi",
                        DB::raw("DATE_FORMAT(barang_masuk.tanggal, '%d-%b-%Y %H:%i') as tanggal_barang_masuk"))
            ->join('barang_masuk', 'barang_masuk.id', '=', 'pergerakan_barang.barang_masuk_id')
            ->join('barang', 'barang.id', '=', 'barang_masuk.barang_id')
            ->join('lokasi', 'lokasi.id', '=', 'barang_masuk.lokasi_id')
            ->where('pergerakan_barang.barang_masuk_id', $id)
            ->where('pergerakan_barang.hapuskah', 0)
            ->where('barang.hapuskah', 0)
            ->where('barang_masuk.hapuskah', 0)
            ->where('lokasi.hapuskah', 0)
            ->groupBy('pergerakan_barang.barang_masuk_id')
            ->orderBy('pergerakan_barang.tanggal', 'desc')
            ->get();
            
        $jumlah = array();
        foreach($array as $key=>$cek_status)
        {
            $id_status = $cek_status['id_status'];
            array_push($jumlah, array("id_status" => $cek_status["id_status"], "jumlah" => array()));

            $temp = DB::select
            (   
                "SELECT l.id, l.nama_lokasi,
                ( 
                    SELECT COUNT(pa.id) 
                    FROM pergerakan_barang pa, lokasi la 
                    where pa.barang_masuk_id = '$id' and pa.status = '$id_status' and pa.hapuskah = 0 and la.hapuskah = 0 and pa.lokasi_id = la.id and la.id = l.id
                ) AS jumlah 
                FROM pergerakan_barang p, lokasi l 
                where p.barang_masuk_id = '$id' and p.status = '$id_status' and p.hapuskah = 0 and l.hapuskah = 0 and p.lokasi_id = l.id 
                GROUP BY p.lokasi_id"
            );

            foreach($temp as $data_temp)
            {
                array_push($jumlah[$key]['jumlah'], array("id_lokasi" => $data_temp->id, "jumlah" => $data_temp->jumlah, "nama_lokasi" => $data_temp->nama_lokasi));
            }
        }

        // dd($jumlah);

        $data = array('data' => $ret, "status" => $status, "data_status" => $jumlah);
        return view('pergerakan/detail')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function status($id, $status, $lokasi)
    {
        //
        $statusnya = Status::where('id', $status)->where('hapuskah', 0)->orderBy('id', 'asc')->get();
        $ret = DB::table('pergerakan_barang')
            ->select(   "barang_masuk.id", "barang.nama_barang", "pergerakan_barang.status", "barang_masuk.jumlah as jumlah_barang_masuk", 
                        DB::raw("DATE_FORMAT(pergerakan_barang.tanggal, '%d-%b-%Y %H:%i') as tanggal"), "lokasi.nama_lokasi",
                        DB::raw("DATE_FORMAT(barang_masuk.tanggal, '%d-%b-%Y %H:%i') as tanggal_barang_masuk"))
            ->join('barang_masuk', 'barang_masuk.id', '=', 'pergerakan_barang.barang_masuk_id')
            ->join('barang', 'barang.id', '=', 'barang_masuk.barang_id')
            ->join('lokasi', 'lokasi.id', '=', 'barang_masuk.lokasi_id')
            ->where('pergerakan_barang.barang_masuk_id', $id)
            ->where('pergerakan_barang.hapuskah', 0)
            ->where('barang.hapuskah', 0)
            ->where('barang_masuk.hapuskah', 0)
            ->where('lokasi.hapuskah', 0)
            ->groupBy('pergerakan_barang.barang_masuk_id')
            ->orderBy('pergerakan_barang.tanggal', 'desc')
            ->get();
            
        $jumlah = DB::select
        (   
            "SELECT l.id, l.nama_lokasi,
            ( 
                SELECT COUNT(pa.id) 
                FROM pergerakan_barang pa, lokasi la 
                where pa.barang_masuk_id = '$id' and pa.status = '$status' and pa.hapuskah = 0 and la.hapuskah = 0 and pa.lokasi_id = la.id and la.id = l.id
            ) AS jumlah 
            FROM pergerakan_barang p, lokasi l 
            where p.barang_masuk_id = '$id' and p.status = '$status' and p.hapuskah = 0 and l.hapuskah = 0 and p.lokasi_id = l.id and l.id = '$lokasi'
            GROUP BY p.lokasi_id"
        );
            
        $list_barang = DB::table('pergerakan_barang')
            ->select("id", DB::raw("DATE_FORMAT(tanggal, '%d-%b-%Y %H:%i') as tanggal"), "catatan")
            ->where('barang_masuk_id', $id)
            ->where('status', $status)
            ->where('lokasi_id', $lokasi)
            ->where('hapuskah', 0)
            ->orderBy('pergerakan_barang.id', 'asc')
            ->get();

        $data = array('data' => $ret, "status" => $statusnya, "data_status" => $jumlah, "list_barang" => $list_barang);

        return view('pergerakan/status')->with($data);
    }

    public function updatestatus($id)
    {
        //
        $ret = DB::table('pergerakan_barang')
        ->select(   "pergerakan_barang.id", DB::raw("DATE_FORMAT(pergerakan_barang.tanggal, '%d-%b-%Y %H:%i') as tanggal"),
                    "barang.nama_barang", "barang_masuk.jumlah as jumlah_barang_masuk", "lokasi.nama_lokasi", "pergerakan_barang.lokasi_id", "pergerakan_barang.catatan",
                    DB::raw("DATE_FORMAT(barang_masuk.tanggal, '%d-%b-%Y %H:%i') as tanggal_barang_masuk"), "pergerakan_barang.status", "status.status as nama_status")
        ->join('barang_masuk', 'barang_masuk.id', '=', 'pergerakan_barang.barang_masuk_id')
        ->join('barang', 'barang.id', '=', 'barang_masuk.barang_id')
        ->join('lokasi', 'lokasi.id', '=', 'pergerakan_barang.lokasi_id')
        ->join('status', 'status.id', '=', 'pergerakan_barang.status')
        ->where('pergerakan_barang.id', $id)
        ->where('pergerakan_barang.hapuskah', 0)
        ->where('barang_masuk.hapuskah', 0)
        ->where('barang.hapuskah', 0)
        ->where('status.hapuskah', 0)
        ->where('lokasi.hapuskah', 0)
        ->get();

        $lokasi_awal = DB::table('pergerakan_barang')
            ->select("lokasi.nama_lokasi")
            ->join('barang_masuk', 'barang_masuk.id', '=', 'pergerakan_barang.barang_masuk_id')
            ->join('lokasi', 'lokasi.id', '=', 'barang_masuk.lokasi_id')
            ->where('pergerakan_barang.id', $id)
            ->where('pergerakan_barang.hapuskah', 0)
            ->where('barang_masuk.hapuskah', 0)
            ->where('lokasi.hapuskah', 0)
            ->get();

        $data_status = Status::where('hapuskah', 0)->orderBy('id', 'asc')->get();
        $data_lokasi = Lokasi::where('hapuskah', 0)->orderBy('nama_lokasi', 'asc')->get();
        $data_history = DB::table('history_barang')
            ->select('history_barang.catatan', 'status.status', 'lokasi.nama_lokasi', DB::raw("DATE_FORMAT(history_barang.tanggal, '%d-%b-%Y %H:%i') as tanggal"))
            ->join('status', 'status.id', '=', 'history_barang.status_akhir')
            ->join('lokasi', 'lokasi.id', '=', 'history_barang.lokasi')
            ->where('history_barang.pergerakan_barang_id', $id)
            ->where('status.hapuskah', 0)
            ->where('lokasi.hapuskah', 0)
            ->orderBy('history_barang.timestamp', 'desc')
            ->get();

        $data = array('data' => $ret, 'data_status' => $data_status, 'data_lokasi' => $data_lokasi, 'lokasi_awal' => $lokasi_awal, 'data_history' => $data_history);

        return view('pergerakan/updatestatus')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $validatedData = $request->validate(
        [
            'id'        => 'required|numeric',
            'tanggal'   => 'required',
            'status'    => 'required|numeric',
            'alamat'    => 'required|numeric',
            'catatan'    => 'max:5000',
        ],
        [
            'id.required' => 'ID Tidak Ditemukan',
            'id.numeric' => 'ID Tidak Ditemukan',
            'tanggal.required' => 'Silahkan Isi Pergerakan Terlebih Dahulu.',
            'status.required' => 'Status Tidak Ditemukan',
            'status.numeric' => 'Status Tidak Ditemukan',
            'alamat.required' => 'Alamat Lokasi Penyimpanan Tidak Ditemukan',
            'alamat.numeric' => 'Alamat Lokasi Penyimpanan Tidak Ditemukan',
            'catatan.max' => 'Catatan Maksimal Terdiri Dari 5000 Karakter',
        ]);
        
        $cek = DB::table('pergerakan_barang')
        ->select("pergerakan_barang.id", "pergerakan_barang.barang_masuk_id", "pergerakan_barang.status", "pergerakan_barang.lokasi_id", "status.status as nama_status")
        ->join('status', 'status.id', '=', 'pergerakan_barang.status')
        ->where('pergerakan_barang.id', $request->id)
        ->where('pergerakan_barang.hapuskah', 0)
        ->where('status.hapuskah', 0);

        if($cek->count())
        {
            $pergerakan = Pergerakan::find($request->id);

            $pergerakan->lokasi_id = $request->alamat;
            $pergerakan->tanggal = date('Y-m-d H:i:00', strtotime($request->tanggal));
            $pergerakan->status = $request->status;
            $pergerakan->catatan = $request->catatan;
            $pergerakan->update();

            $cek = $cek->first();

            $history = new HistoryBarang;
            $history->pergerakan_barang_id = $request->id;
            $history->status_akhir = $request->status;
            $history->lokasi = $request->alamat;
            $history->tanggal = date('Y-m-d H:i:00', strtotime($request->tanggal));
            if($request->catatan != "")
            { $history->catatan = $request->catatan; }
            $history->save();

            // dd($cek->barang_masuk_id, $cek->status, $cek->lokasi_id);
            return redirect('/pergerakan/status/'.$cek->barang_masuk_id.'/'.$cek->status.'/'.$cek->lokasi_id);
        }
        else
        {
            return redirect()->route('index_pergerakan');
        }
    }

    public function qrcode($id)
    {
        //
        $return = array();

        $pergerakan = Pergerakan::find($id);
        if($pergerakan)
        {
            $qrcode = QrCode::size(200)->generate(url('detailbarang/'.$id));   
            $return = array('status' => true, 'qrcode' => $qrcode);
            echo json_encode($return);
        }
        else
        {
            $return = array('status' => false, 'pesan' => 'ID Barang Tidak Ditemukan!');
            echo json_encode($return);
        }
    }

    public function cetak_qrcode($id)
    {
        $data = array();
        $pergerakan = Pergerakan::find($id);
        
        if($pergerakan)
        {
            $qrcode = QrCode::size(200)->generate(url('detailbarang/'.$id));   
            $data = array('status' => true, 'qrcode' => $qrcode);
        }
        else
        {
            $data = array('status' => false);
        }
        return view('pergerakan/cetak_qrcode')->with($data);
    }
}
