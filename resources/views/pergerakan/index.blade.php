@extends('layouts.index')

@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">PERGERAKAN BARANG</h1>
			</div>
		</div><!-- /.row -->
		<div class="row mb-2 keterangan_status">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Keterangan Status:</h1>
                    </div>
                </div>
                <div class="row">

                @foreach ($data_status as $status)
                
                    <div class="col-md-2">
                        <h4>S<span class="sub">{{ ($status->id + 1) }}</span> :<br>{{ $status->status }}</h4>
                    </div>
                    
                @endforeach

                </div>
            </div>
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-primary card-outline">
					<div class="card-body">
						<table id="dataTablesPergerakan" class="display table" width="100%">
							<thead>
								<tr>
									<th width="50px">No</th>
									<th width="150px">Tanggal</th>
									<th>Nama Barang</th>
									<th width="80px">Jumlah</th>

                                    @foreach ($data_status as $status)
                                    
                                    <th width="50px">S<span class="sub">{{ ($status->id + 1) }}</span></th>
                                        
                                    @endforeach
                                    
									<th width="10px"></th>
								</tr>
							</thead>
							<tbody>
							
                            @foreach ($data as $datas)
								<tr>
									<td>{{ ($loop->index + 1) }}</td>
									<td>{{ $datas['tanggal'] }}</td>
									<td>{{ $datas['nama_barang'] }}</td>
                                    <td>{{ $datas['jumlah'] }}</td>
                                    
                                    @foreach ($datas['data_pergerakan'] as $jumlah)
                                        
                                    <td>{{ $jumlah['jumlah'] }}</td>
                                    
                                    @endforeach
                                    
									<td><a href="{{ url('pergerakan/detail/') }}/{{$datas['id']}}" class="btn btn-info btn-sm"><i class="fas fa-edit"></i></a></td>
								</tr>
							@endforeach

							</tbody>
                        </table>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<script>	
function hapus(id, nama)
{
    swal(
    {
        title: "PERHATIAN!",
        text: "Apakah Anda Yakin Ingin Menghapus\nData Pergerakan "+nama+" ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => 
    {
        if (willDelete) 
        {            
            $.ajax(
            {
                type:'post',
                dataType:'json',
                data: {"id": id, "_token": "{{ csrf_token() }}"},
                url: APP_URL+'/pergerakan/hapus',
            })
        
            .done(function(data)
            {	
                if(data.status)
                {                    
                    swal("Data Berhasil Dihapus!", 
                    {
                        icon: "success",
                    }).then((value) => { location.reload(); });;
                }
                else
                {
                    swal( 
                    {
                        title: "PERHATIAN!",
                        text: data.pesan,
                        icon: "warning",
                        dangerMode: true,
                    });
                }
            })
        
            .error(function()
            {
                swal( 
                {
                    title: "PERHATIAN!",
                    text: "Terjadi Kesalahan Teknis! Silahkan Coba Lagi.",
                    icon: "warning",
                    dangerMode: true,
                });
            });
        } 
    });
}
</script>
  <!-- /.content -->
@endsection
