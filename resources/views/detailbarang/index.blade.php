<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>PT. TMNI</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ asset('AdminLTE/plugins/fontawesome-free/css/all.min.css')}}">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Tempusdominus Bbootstrap 4 -->
        <link rel="stylesheet" href="{{ asset('AdminLTE/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
        <!-- iCheck -->
        <link rel="stylesheet" href="{{ asset('AdminLTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
        <!-- JQVMap -->
        <link rel="stylesheet" href="{{ asset('AdminLTE/plugins/jqvmap/jqvmap.min.css')}}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ asset('AdminLTE/dist/css/adminlte.min.css')}}">
        <!-- overlayScrollbars -->
        <link rel="stylesheet" href="{{ asset('AdminLTE/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="{{ asset('AdminLTE/plugins/daterangepicker/daterangepicker.css')}}">
        <!-- summernote -->
        <link rel="stylesheet" href="{{ asset('AdminLTE/plugins/summernote/summernote-bs4.css')}}">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        <link rel="stylesheet" href="{{ URL::asset('css/css.css') }}">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .title {
                text-align: center;
                padding-top: 50px;
                font-size: 54px;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .row{
                margin: 0;
            }

            b
            {
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    Detail Barang
                </div>

                
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="row justify-content-sm-center">
                <div class="col-md-6">
                    <div class="card card-info card-outline">
                        <div class="card-header">
                            <h4>Data Barang Masuk</h4>
                        </div>
                        <div class="card-body">

                            <div class="row justify-content-sm-center">
                                <div class="col-sm-4 justify-content-sm-end text-right">
                                    <div>
                                        <label>Nama Barang:</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div>
                                        <span>{{ $data[0]->nama_barang }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-sm-center">
                                <div class="col-sm-4 justify-content-sm-end text-right">
                                    <div>
                                        <label>Tanggal Barang Masuk:</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div>
                                        <span>{{ $data[0]->tanggal_barang_masuk }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-sm-center">
                                <div class="col-sm-4 justify-content-sm-end text-right">
                                    <div>
                                        <label>Jumlah Barang Masuk:</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div>
                                        <span>{{ $data[0]->jumlah_barang_masuk }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-sm-center">
                                <div class="col-sm-4 justify-content-sm-end text-right">
                                    <div>
                                        <label>Lokasi Awal:</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div>
                                        <span>{{ $lokasi_awal[0]->nama_lokasi }}</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row justify-content-sm-center">
                <div class="col-md-6">
                    <div class="card card-info card-outline">

                        <div class="card-header">
                            <h4>Data Barang {{ $data[0]->nama_barang }}</h4>
                        </div>

                        <div class="card-body">

                            <div class="row justify-content-sm-center">
                                <div class="col-sm-4 justify-content-sm-end text-right">
                                    <div>
                                        <label>ID:</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div>
                                        <span>{{ $data[0]->id }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-sm-center">
                                <div class="col-sm-4 justify-content-sm-end text-right">
                                    <div>
                                        <label>Status:</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div>
                                        <span>{{ $data[0]->nama_status }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-sm-center">
                                <div class="col-sm-4 justify-content-sm-end text-right">
                                    <div>
                                        <label>Tanggal:</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div>
                                        <span>{{ $data[0]->tanggal }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-sm-center">
                                <div class="col-sm-4 justify-content-sm-end text-right">
                                    <div>
                                        <label>Lokasi Barang:</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div>
                                        <span>{{ $data[0]->nama_lokasi }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-sm-center">
                                <div class="col-sm-4 justify-content-sm-end text-right">
                                    <div>
                                        <label>Catatan:</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div>
                                        <span>{{ $data[0]->catatan == "" ? "-" : $data[0]->catatan }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row justify-content-sm-center">
                    <div class="col-md-6">
                        <div class="card card-info card-outline">

                            <div class="card-header">
                                <h4>History Barang</h4>
                            </div>

                            <div class="card-body">

                                <div class="timeline">

                                    @php
                                    $temp_tanggal = "";   
                                    @endphp
                                    @foreach ($data_history as $key=>$history)
                                        @if($temp_tanggal != date('d M Y', strtotime($history->tanggal)))                                      
                                            @php
                                                $temp_tanggal = date('d M Y', strtotime($history->tanggal));   
                                            @endphp

                                    <div class="time-label">
                                        <span class="bg-info">{{ $temp_tanggal }}</span>
                                    </div>

                                        @endif
                                    <div>
                                        <i class="fas fa-pallet bg-info"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fas fa-clock"></i> {{ date('H:i', strtotime($history->tanggal)) }}</span>
                                            <h3 class="timeline-header"><b>{{ ($key+1) == count($data_history) ? "Barang Baru Masuk" : "Update Status Barang" }}</b></h3>
                        
                                            <div class="timeline-body">
                                                Barang <b>{{ $history->status }}</b>
                                                <br>
                                                Lokasi Barang Di <b>{{ $history->nama_lokasi}}</b>
                                                <br><br>

                                                <b>Catatan:</b> <br>
                                                {{ $history->catatan != "" ? $history->catatan : '-' }}
                                            </div>

                                            <div class="timeline-footer" style="font-size: 13px; color: grey;">
                                                Oleh User:
                                            </div>
                                        </div>
                                    </div>
                                        
                                    @endforeach

                                    <div>
                                        <i class="fas fa-stop-circle bg-gray"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</section>
            </div>
        </div>
    </body>
</html>
