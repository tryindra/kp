<?php

namespace App\Http\Controllers;

use App\Lokasi;
use Illuminate\Http\Request;

class LokasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $ret = Lokasi::where('hapuskah', 0)->get();
        $data = array('data' => $ret);
        return view('lokasi/index')->with($data);
    }

    public function load_datatable()
    {
        //
        $ret = Lokasi::all();
        $data = array('data' => $ret);
        return view('lokasi/index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('lokasi/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $request->validate(
        [
            'nama_lokasi' => 'required|string|max:255|unique:lokasi,nama_lokasi',
            'alamat' => 'required|string|max:255',
        ],
        [
            'nama_lokasi.required' => 'Silahkan Isi Lokasi Terlebih Dahulu.',
            'nama_lokasi.unique' => 'Lokasi Telah Tercatat. Silahkan Masukkan Lokasi Lainnya.',
            'nama_lokasi.max' => 'Maksimal Terdiri Dari 255 Karakter.',
            'nama_lokasi.string' => 'Inputan Harus Berupa Teks Huruf/Angka',

            'alamat.required' => 'Silahkan Isi Alamat Lokasi Terlebih Dahulu.',
            'alamat.max' => 'Maksimal Terdiri Dari 255 Karakter.',
            'alamat.string' => 'Inputan Harus Berupa Teks Huruf/Angka',
        ]);

        $lokasi = new Lokasi;
        $lokasi->nama_lokasi = $request->nama_lokasi;
        $lokasi->alamat_lokasi = $request->alamat;
        $lokasi->deskripsi = $request->deskripsi;
        $lokasi->hapuskah = false;
        $lokasi->status = false;
        if($request->status )
        {
            $lokasi->status = true;
        }
        $lokasi->save();

        return redirect()->route('index_lokasi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $ret = Lokasi::find($id);

        if($ret)
        {
            $data = array('data' => $ret);
            return view('lokasi/edit', ['data' => $data]);
        }
        else
        {
            return redirect()->route('index_lokasi');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $validatedData = $request->validate(
        [
            'nama_lokasi' => 'required|string|max:255',
            'alamat' => 'required|string|max:255',
            'id' => 'required',
        ],
        [
            'id.required' => 'ID Tidak Ditemukan',
            'nama_lokasi.required' => 'Silahkan Isi Lokasi Terlebih Dahulu.',
            'nama_lokasi.unique' => 'Lokasi Telah Tercatat. Silahkan Masukkan Lokasi Lainnya.',
            'nama_lokasi.max' => 'Maksimal Terdiri Dari 255 Karakter.',
            'nama_lokasi.string' => 'Inputan Harus Berupa Teks Huruf/Angka',

            'alamat.required' => 'Silahkan Isi Alamat Lokasi Terlebih Dahulu.',
            'alamat.max' => 'Maksimal Terdiri Dari 255 Karakter.',
            'alamat.string' => 'Inputan Harus Berupa Teks Huruf/Angka',
        ]);
        
        $cek = Lokasi::where('nama_lokasi', $request->kategori_barang)->where('id', '<>', $request->id)->count();
        if($cek)
        {
            return redirect('/lokasi/edit/'.$request->id)->withErrors(['kategori_barang' => 'Lokasi Telah Tercatat. Silahkan Masukkan Lokasi Lainnya.']); 
        }
        else
        {
            $lokasi = Lokasi::find($request->id);
            $lokasi->nama_lokasi = $request->nama_lokasi;
            $lokasi->alamat_lokasi = $request->alamat;
            $lokasi->deskripsi = $request->deskripsi;
            $lokasi->hapuskah = false;
            $lokasi->status = false;
            if($request->status )
            {
                $lokasi->status = true;
            }
            $lokasi->update();

            return redirect()->route('index_lokasi');
        }
    }

    public function hapus(Request $request)
    {
        $validatedData = $request->validate(
        [
            'id' => 'required',
        ],
        [
            'id.required' => 'ID Tidak Ditemukan',
        ]);

        $lokasi = Lokasi::find($request->id);
        $lokasi->hapuskah = true;
        $update = $lokasi->update();

        if($update)
        {
            return json_encode(array('status' => true));
        }
        else
        {
            return json_encode(array('status' => false, 'pesan' => 'Data Gagal Dihapus! Silahkan Coba Lagi.'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
