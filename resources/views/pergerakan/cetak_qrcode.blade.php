<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<style>
    .content
    {
        display: flex;
        align-items: center;
        justify-content: center;
        width: 100%;
        height: 100vh;
    }
    body
    {
        margin: 0;
        padding: 0;
    }
</style>
<body>
    <div class="content">

    @if ($status)
        {!! $qrcode !!}
    @else
        Barang Tidak Ditemukan
    @endif

    </div>
</body>

<script>
    window.print();
</script>
</html>