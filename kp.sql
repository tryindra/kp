-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 20, 2020 at 01:22 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kp`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id` int(11) NOT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `kategori_id` int(11) NOT NULL,
  `deskripsi` text DEFAULT NULL,
  `gambar_barang` longtext DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT current_timestamp(),
  `hapuskah` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id`, `nama_barang`, `kategori_id`, `deskripsi`, `gambar_barang`, `timestamp`, `hapuskah`, `status`) VALUES
(1, 'BARANG A', 10, 'Keterangan', '', '2020-06-27 17:31:48', 0, 1),
(12, 'BARANG B', 6, NULL, '', '2020-07-16 14:55:33', 0, 1),
(13, 'BARANG C', 6, 'Deskripsi', '', '2020-07-18 07:03:15', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `barang_masuk`
--

CREATE TABLE `barang_masuk` (
  `id` int(11) NOT NULL,
  `barang_id` int(11) NOT NULL,
  `lokasi_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deskripsi` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp(),
  `hapuskah` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang_masuk`
--

INSERT INTO `barang_masuk` (`id`, `barang_id`, `lokasi_id`, `jumlah`, `tanggal`, `deskripsi`, `timestamp`, `hapuskah`) VALUES
(1, 1, 1, 10, '2020-07-17 00:00:00', 'Contoh Keterangan', '2020-07-18 07:23:57', 0);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `history_barang`
--

CREATE TABLE `history_barang` (
  `id` int(11) NOT NULL,
  `pergerakan_barang_id` int(11) NOT NULL,
  `status_akhir` text NOT NULL,
  `lokasi` text NOT NULL,
  `catatan` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp(),
  `tanggal` datetime NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `history_barang`
--

INSERT INTO `history_barang` (`id`, `pergerakan_barang_id`, `status_akhir`, `lokasi`, `catatan`, `timestamp`, `tanggal`, `user_id`) VALUES
(1, 1, '0', '1', '', '2020-07-18 07:23:57', '2020-07-17 07:00:00', 0),
(2, 2, '0', '1', '', '2020-07-18 07:23:57', '2020-07-17 07:00:00', 0),
(3, 3, '0', '1', '', '2020-07-18 07:23:57', '2020-07-17 07:00:00', 0),
(4, 4, '0', '1', '', '2020-07-18 07:23:57', '2020-07-17 07:00:00', 0),
(5, 5, '0', '1', '', '2020-07-18 07:23:57', '2020-07-17 07:00:00', 0),
(6, 6, '0', '1', '', '2020-07-18 07:23:58', '2020-07-17 07:00:00', 0),
(7, 7, '0', '1', '', '2020-07-18 07:23:58', '2020-07-17 07:00:00', 0),
(8, 8, '0', '1', '', '2020-07-18 07:23:58', '2020-07-17 07:00:00', 0),
(9, 9, '0', '1', '', '2020-07-18 07:23:58', '2020-07-17 07:00:00', 0),
(10, 10, '0', '1', '', '2020-07-18 07:23:58', '2020-07-17 07:00:00', 0),
(11, 1, '4', '1', '', '2020-07-18 07:24:38', '2020-07-17 10:00:00', 0),
(12, 1, '1', '1', 'Rusak Di Bagian Samping & Depan', '2020-07-18 07:25:16', '2020-07-18 08:00:00', 0),
(13, 5, '4', '2', '', '2020-07-18 07:26:16', '2020-07-18 12:00:00', 0),
(14, 5, '5', '1', '', '2020-07-18 11:46:36', '2020-07-18 18:00:00', 0),
(15, 2, '4', '1', 'sedang di simpan', '2020-07-19 08:01:55', '2020-07-19 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `nama_kategori` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `timestamp` timestamp NULL DEFAULT current_timestamp(),
  `hapuskah` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `nama_kategori`, `deskripsi`, `timestamp`, `hapuskah`) VALUES
(6, 'Makanan', 'Keterangan Makanan', '2020-06-27 15:50:48', 0),
(7, 'tes', 'asdasd', '2020-06-27 15:52:52', 1),
(8, 'tesaa2', 'asdasd', '2020-06-27 16:38:12', 1),
(9, 'Makanan 2', 'Keterangan', '2020-06-29 14:37:23', 1),
(10, 'Minuman', 'Keterangan/Deskripsi', '2020-07-04 13:52:09', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lokasi`
--

CREATE TABLE `lokasi` (
  `id` int(11) NOT NULL,
  `nama_lokasi` varchar(255) DEFAULT NULL,
  `alamat_lokasi` longtext DEFAULT NULL,
  `deskripsi` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` tinyint(1) NOT NULL,
  `hapuskah` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lokasi`
--

INSERT INTO `lokasi` (`id`, `nama_lokasi`, `alamat_lokasi`, `deskripsi`, `timestamp`, `status`, `hapuskah`) VALUES
(1, 'GUDANG A', 'Alamat GUDANG A', 'Keterangan', '2020-06-30 14:37:21', 1, 0),
(2, 'GUDANG B', 'Alamat GUDANG B', 'Keterangan', '2020-06-30 14:40:00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pergerakan_barang`
--

CREATE TABLE `pergerakan_barang` (
  `id` int(11) NOT NULL,
  `barang_masuk_id` int(11) NOT NULL,
  `lokasi_id` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` varchar(100) NOT NULL,
  `catatan` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `hapuskah` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pergerakan_barang`
--

INSERT INTO `pergerakan_barang` (`id`, `barang_masuk_id`, `lokasi_id`, `tanggal`, `status`, `catatan`, `timestamp`, `hapuskah`) VALUES
(1, 1, 1, '2020-07-18 01:00:00', '1', 'Rusak Di Bagian Samping & Depan', '2020-07-18 07:25:16', 0),
(2, 1, 1, '2020-07-18 17:00:00', '4', 'sedang di simpan', '2020-07-19 08:01:55', 0),
(3, 1, 1, '2020-07-17 00:00:00', '0', 'Barang Baru Masuk', '2020-07-18 07:23:57', 0),
(4, 1, 1, '2020-07-17 00:00:00', '0', 'Barang Baru Masuk', '2020-07-18 07:23:57', 0),
(5, 1, 1, '2020-07-18 11:00:00', '5', '', '2020-07-18 11:46:36', 0),
(6, 1, 1, '2020-07-17 00:00:00', '0', 'Barang Baru Masuk', '2020-07-18 07:23:58', 0),
(7, 1, 1, '2020-07-17 00:00:00', '0', 'Barang Baru Masuk', '2020-07-18 07:23:58', 0),
(8, 1, 1, '2020-07-17 00:00:00', '0', 'Barang Baru Masuk', '2020-07-18 07:23:58', 0),
(9, 1, 1, '2020-07-17 00:00:00', '0', 'Barang Baru Masuk', '2020-07-18 07:23:58', 0),
(10, 1, 1, '2020-07-17 00:00:00', '0', 'Barang Baru Masuk', '2020-07-18 07:23:58', 0);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `hapuskah` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `status`, `hapuskah`, `created_at`) VALUES
(0, 'Sedang Disimpan', 0, '2020-07-18 07:05:53'),
(1, 'Sedang Rusak', 0, '2020-07-18 07:05:56'),
(2, 'Sedang Diperbaiki', 0, '2020-07-18 07:05:57'),
(3, 'Selesai Diperbaiki', 0, '2020-07-18 07:05:58'),
(4, 'Sedang Digunakan', 0, '2020-07-18 07:06:00'),
(5, 'Selesai Digunakan', 0, '2020-07-18 07:06:01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', NULL, '$2y$10$7BpJnfXjH.0DSazAtnrrNuJS.6QYiwdZLi6KkDW2hJsFaTSuapOta', NULL, '2020-06-24 07:00:59', '2020-06-24 07:00:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kategori_id` (`kategori_id`);

--
-- Indexes for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history_barang`
--
ALTER TABLE `history_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lokasi`
--
ALTER TABLE `lokasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pergerakan_barang`
--
ALTER TABLE `pergerakan_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `history_barang`
--
ALTER TABLE `history_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `lokasi`
--
ALTER TABLE `lokasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pergerakan_barang`
--
ALTER TABLE `pergerakan_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=223;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `barang`
--
ALTER TABLE `barang`
  ADD CONSTRAINT `barang_ibfk_1` FOREIGN KEY (`kategori_id`) REFERENCES `kategori` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
