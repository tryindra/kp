<?php

namespace App\Http\Controllers;

use App\BarangMasuk;
use App\Barang;
use App\Lokasi;
use App\Pergerakan;
use App\HistoryBarang;
use Illuminate\Http\Request;
use DB;

class BarangMasukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $ret = DB::table('barang_masuk')
                ->select(   "barang_masuk.id", "barang.nama_barang", "barang_masuk.jumlah", "lokasi.nama_lokasi",
                            DB::raw("DATE_FORMAT(barang_masuk.tanggal, '%d-%b-%Y %H:%i') as tanggal"))
                ->join('barang', 'barang.id', '=', 'barang_masuk.barang_id')
                ->join('lokasi', 'lokasi.id', '=', 'barang_masuk.lokasi_id')
                ->where('barang.hapuskah', 0)
                ->where('barang_masuk.hapuskah', 0)
                ->where('lokasi.hapuskah', 0)
                ->orderBy('barang_masuk.tanggal', 'desc')
                ->get();
        $data = array('data' => $ret);
        return view('barang_masuk/index')->with($data);
    }

    public function load_datatable()
    {
        //
        $ret = BarangMasuk::all();
        $data = array('data' => $ret);
        return view('barang_masuk/index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data_barang = Barang::where('hapuskah', 0)->where('status', 1)->get();
        $data_lokasi = Lokasi::where('hapuskah', 0)->where('status', 1)->get();
        $data = array('data_barang' => $data_barang, 'data_lokasi' => $data_lokasi);
        
        return view('barang_masuk/create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $request->validate(
        [
            'nama_barang' => 'required|string|max:255',
            'alamat' => 'required|string|max:255',
            'jumlah' => 'required|numeric',
        ],
        [
            'nama_barang.required' => 'Silahkan Isi BarangMasuk Terlebih Dahulu.',
            'nama_barang.max' => 'Maksimal Terdiri Dari 255 Karakter.',
            'nama_barang.string' => 'Inputan Harus Berupa Teks Huruf/Angka',

            'alamat.required' => 'Silahkan Isi Alamat BarangMasuk Terlebih Dahulu.',
            'alamat.max' => 'Maksimal Terdiri Dari 255 Karakter.',
            'alamat.string' => 'Inputan Harus Berupa Teks Huruf/Angka',

            'jumlah.required' => 'Silahkan Isi Jumlah Barang Masuk Terlebih Dahulu.',
            'jumlah.numeric' => 'Inputan Harus Berupa Teks Angka',
        ]);

        $barang_masuk = new BarangMasuk;
        $barang_masuk->barang_id = $request->nama_barang;
        $barang_masuk->jumlah = $request->jumlah;
        $barang_masuk->tanggal = date('Y-m-d H:i:00', strtotime($request->tanggal));
        $barang_masuk->lokasi_id = $request->alamat;
        $barang_masuk->deskripsi = $request->deskripsi;
        $barang_masuk->hapuskah = false;
        
        $barang_masuk->save();

        for($a = 0; $a < $request->jumlah; $a++)
        {
            $pergerakan = new Pergerakan;
            $pergerakan->barang_masuk_id = $barang_masuk->id;
            $pergerakan->lokasi_id = $request->alamat;
            $pergerakan->tanggal = date('Y-m-d H:i:00', strtotime($request->tanggal));
            $pergerakan->status = 0;
            $pergerakan->catatan = "Barang Baru Masuk";
            $pergerakan->hapuskah = false;
            $pergerakan->save();

            $history = new HistoryBarang;
            $history->pergerakan_barang_id = $pergerakan->id;
            $history->status_akhir = 0;
            $history->lokasi = $request->alamat;
            $history->tanggal = date('Y-m-d H:i:00', strtotime($request->tanggal));
            $history->save();
        }
        
        $barang_masuk->save();

        return redirect()->route('index_barang_masuk');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $ret = BarangMasuk::find($id);

        if($ret)
        {
            $ret = DB::table('barang_masuk')
                    ->select(   "barang_masuk.id", "barang.nama_barang", "lokasi.nama_lokasi", "barang_masuk.deskripsi", "barang_masuk.jumlah", 
                                DB::raw("DATE_FORMAT(barang_masuk.tanggal, '%d-%b-%Y %H:%i') as tanggal"))
                    ->join('barang', 'barang.id', '=', 'barang_masuk.barang_id')
                    ->join('lokasi', 'lokasi.id', '=', 'barang_masuk.lokasi_id')
                    ->where('barang_masuk.id', $id)
                    ->where('barang.hapuskah', 0)
                    ->where('barang_masuk.hapuskah', 0)
                    ->where('lokasi.hapuskah', 0)
                    ->get();
            // dd($ret);
            return view('barang_masuk/edit', ['data' => $ret]);
        }
        else
        {
            return redirect()->route('index_barang_masuk');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $validatedData = $request->validate(
        [
            'deskripsi' => 'required|string|max:255',
            'id' => 'required',
        ],
        [
            'id.required' => 'ID Tidak Ditemukan',
            'deskripsi.required' => 'Silahkan Isi BarangMasuk Terlebih Dahulu.',
            'deskripsi.max' => 'Maksimal Terdiri Dari 255 Karakter.',
            'deskripsi.string' => 'Inputan Harus Berupa Teks Huruf/Angka',
        ]);
        
        $barang_masuk = BarangMasuk::find($request->id);
        $barang_masuk->deskripsi = $request->deskripsi;
        $barang_masuk->update();

        return redirect()->route('index_barang_masuk');
    }

    public function hapus(Request $request)
    {
        $validatedData = $request->validate(
        [
            'id' => 'required',
        ],
        [
            'id.required' => 'ID Tidak Ditemukan',
        ]);

        $barang_masuk = BarangMasuk::find($request->id);
        $barang_masuk->hapuskah = true;
        $update = $barang_masuk->update();

        if($update)
        {
            return json_encode(array('status' => true));
        }
        else
        {
            return json_encode(array('status' => false, 'pesan' => 'Data Gagal Dihapus! Silahkan Coba Lagi.'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
