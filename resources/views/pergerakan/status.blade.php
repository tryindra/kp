@extends('layouts.index')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
                <h1 class="m-0 text-dark">
                    <a href="{{ url('pergerakan/detail/') }}/{{ $data[0]->id }}" style="font-size: 24px; margin-right: 10px;"><i class="fas fa-chevron-left"></i></a>
                    DETAIL PERGERAKAN BARANG
                </h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
		<div class="row mb-2">
		</div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
                    <div class="col-md-6">
                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h4>Data Status Barang</h4>
                            </div>
                            <div class="card-body">

                                @foreach($data as $datas)

                                <div class="row justify-content-sm-center">
                                    <div class="col-sm-4 justify-content-sm-end text-right">
                                        <div>
                                            <label>Status Barang:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div>
                                            <span>{{ $status[0]->status }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-sm-center">
                                    <div class="col-sm-4 justify-content-sm-end text-right">
                                        <div>
                                            <label>Lokasi Penyimpanan:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div>
                                            <span>{{ $data_status[0]->nama_lokasi }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-sm-center">
                                    <div class="col-sm-4 justify-content-sm-end text-right">
                                        <div>
                                            <label>Jumlah Barang:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div>
                                            <span>{{ $data_status[0]->jumlah }}</span>
                                        </div>
                                    </div>
                                </div>

                                @endforeach

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h4>Data Barang Masuk</h4>
                            </div>
                            <div class="card-body">

                                @foreach($data as $datas)

                                <div class="row justify-content-sm-center">
                                    <div class="col-sm-4 justify-content-sm-end text-right">
                                        <div>
                                            <label>Nama Barang:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div>
                                            <span>{{ $datas->nama_barang }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-sm-center">
                                    <div class="col-sm-4 justify-content-sm-end text-right">
                                        <div>
                                            <label>Tanggal Barang Masuk:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div>
                                            <span>{{ $datas->tanggal_barang_masuk }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-sm-center">
                                    <div class="col-sm-4 justify-content-sm-end text-right">
                                        <div>
                                            <label>Jumlah Barang Masuk:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div>
                                            <span>{{ $datas->jumlah_barang_masuk }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-sm-center">
                                    <div class="col-sm-4 justify-content-sm-end text-right">
                                        <div>
                                            <label>Lokasi Awal:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div>
                                            <span>{{ $datas->nama_lokasi }}</span>
                                        </div>
                                    </div>
                                </div>

                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
					
				<div class="row">
                @foreach ($status as $datas_status)
                    @php
                        $qrcode = QrCode::size(200)->generate('W3Adda Laravel Tutorial');   
                    @endphp

					<div class="col-md-12">
						<div class="card card-info card-outline">
							<div class="card-header">
                                <h4>List Barang {{ $datas_status->status }}</h4>
							</div>
							<div class="card-body">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th width="10px">#</th>
											<th width="200px">ID</th>
											<th width="180px">Tanggal</th>
                                            <th>Catatan</th>
                                            <th width="100px">QRCode</th>
											<th width="10px">#</th>
										</tr>
									</thead>
									<tbody>
                                    
                                    @if (count($status) == 0)
                                    <tr class="belum_ada_data">
                                        <td colspan="4">- belum ada data -</td>
                                    </tr>
                                    @else
                                        @foreach ($list_barang as $list_barangs)	
                                    <tr>						
                                        <td>{{ ($loop->index + 1) }}</td>
                                        <td>{{ $list_barangs->id }}</td>
                                        <td>{{ $list_barangs->tanggal }}</td>
                                        <td>{{ $list_barangs->catatan == "" ? "-" : $list_barangs->catatan }}</td>
                                        <td><button onclick="qrcode({{ $list_barangs->id }})" class="btn btn-sm btn-success">QRCode</button></td>
                                        <td><a href="{{ url('pergerakan/updatestatus/') }}/{{$list_barangs->id}}" class="btn btn-info btn-sm"><i class="fas fa-edit"></i></a></td>
                                    </tr>
                                        @endforeach
                                    @endif
									
									</tbody>

								</table>
							</div>
						</div>
					</div>
				@endforeach

				</div>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

@endsection
