@extends('layouts.index')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">MASTER BARANG</h1>
			</div><!-- /.col -->
			<div class="col-sm-6 text-right">
				<a href="{{ url('barang/create') }}" class="btn btn-info"><i class="fas fa-plus"></i></a>
			</div><!-- /.col -->	
		</div><!-- /.row -->
		<div class="row mb-2">
		</div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-primary card-outline">
					<div class="card-body">
						<table id="dataTablesMasterBarang" class="display table" width="100%">
							<thead>
								<tr>
									<th width="50px">No</th>
									<th>Nama Barang</th>
									<th>Kategori</th>
									<th width="120px">Status</th>
									<th width="10px"></th>
									<th width="10px"></th>
								</tr>
							</thead>
							<tbody>
							@foreach ($data as $datas)
								<tr>
									<td>{{ ($loop->index + 1) }}</td>
									<td>{{ $datas->nama_barang }}</td>
									<td>{{ $datas->nama_kategori }}</td>
									<td>
									@if ($datas->status)
										<div class="alert alert-success" style="padding: 3px 10px;">
											<strong><i class="fas fa-check-square"></i> Aktif</strong>
										</div>
									@else
										<div class="alert alert-warning" style="padding: 3px 10px;">
											<strong><i class="fas fa-window-close"></i> Tidak Aktif</strong>
										</div>
									@endif
									</td>
									<td><a href="{{ url('barang/edit/') }}/{{$datas->id}}" class="btn btn-info btn-sm"><i class="fas fa-pen"></i></a></td>
									<td><button class="btn btn-danger btn-sm" onclick="hapus('{{$datas->id}}', '{{$datas->nama_barang}}')"><i class="fas fa-trash"></i></button></td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<script>
function hapus(id, nama)
{
	swal(
	{
		title: "PERHATIAN!",
		text: "Apakah Anda Yakin Ingin Menghapus\nData Barang "+nama+" ?",
		icon: "warning",
		buttons: true,
		dangerMode: true,
	})
	.then((willDelete) => 
	{
		if (willDelete) 
		{            
			$.ajax(
			{
				type:'post',
				dataType:'json',
				data: {"id": id, "_token": "{{ csrf_token() }}"},
				url: APP_URL+'/barang/hapus',
			})
		
			.done(function(data)
			{	
				if(data.status)
				{                    
					swal("Data Berhasil Dihapus!", 
					{
						icon: "success",
					}).then((value) => { location.reload(); });;
				}
				else
				{
					swal( 
					{
						title: "PERHATIAN!",
						text: data.pesan,
						icon: "warning",
						dangerMode: true,
					});
				}
			})
		
			.error(function()
			{
				swal( 
				{
					title: "PERHATIAN!",
					text: "Terjadi Kesalahan Teknis! Silahkan Coba Lagi.",
					icon: "warning",
					dangerMode: true,
				});
			});
		} 
	});
}
</script>
 
<!-- /.content -->
@endsection
