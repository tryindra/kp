<?php

namespace App\Http\Controllers;

use App\Status;
use DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        
        $data_barang_masuk = DB::table('barang_masuk')
                ->select(   "barang_masuk.id", "barang.nama_barang", "barang_masuk.jumlah", 
                            DB::raw("DATE_FORMAT(barang_masuk.tanggal, '%d-%b-%Y %H:%i') as tanggal"))
                ->join('barang', 'barang.id', '=', 'barang_masuk.barang_id')
                ->where(DB::raw("DATE_FORMAT(barang_masuk.tanggal, '%m')"), date('m'))
                ->where('barang.hapuskah', 0)
                ->where('barang_masuk.hapuskah', 0)
                ->orderBy('barang_masuk.tanggal', 'desc')
                ->get();
                
        $status = Status::where('hapuskah', 0)->whereIn('id', [0, 2, 1, 4])->orderBy('id', 'asc')->get();
        $array = array();
        foreach($status as $list_status)
        {
            array_push($array, array("id_status" => $list_status->id, "status" => $list_status->status));
        }

        $data_barang = array();
        $jumlah = array();
        foreach($array as $key=>$cek_status)
        {
            $id_status = $cek_status['id_status'];
            array_push($jumlah, array("status" => $cek_status["status"], "jumlah" => array()));

            $temp = DB::table('pergerakan_barang')
            ->where(DB::raw("DATE_FORMAT(pergerakan_barang.tanggal, '%m')"), date('m'))
            ->where('status', $id_status)
            ->where('hapuskah', 0)
            ->count();
            
            $jumlah[$key]['jumlah'] = $temp;
        }

        $data = array('jumlah_barang_masuk' => count($data_barang_masuk), "jumlah" => $jumlah, 'bulan_sekarang' => $bulan[(date('n') - 1)].' '.date('Y'));

        return view('home')->with($data);
    }
}
