<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();
Route::get('/', 'Auth\LoginController@showLoginForm');


Route::get('/home', 'HomeController@index')->name('home');

Route::get('/kategori', 'KategoriController@index')->name('index_kategori');
Route::get('/kategori/create', 'KategoriController@create');
Route::get('/kategori/edit/{id}', 'KategoriController@edit');
Route::post('/kategori/store', 'KategoriController@store');
Route::post('/kategori/update', 'KategoriController@update');
Route::post('/kategori/hapus', 'KategoriController@hapus');

Route::get('/barang', 'BarangController@index')->name('index_barang');
Route::get('/barang/create', 'BarangController@create');
Route::post('/barang/store', 'BarangController@store');
Route::get('/barang/edit/{id}', 'BarangController@edit');
Route::post('/barang/update', 'BarangController@update');
Route::post('/barang/hapus', 'BarangController@hapus');

Route::get('/lokasi', 'LokasiController@index')->name('index_lokasi');
Route::get('/lokasi/create', 'LokasiController@create');
Route::get('/lokasi/edit/{id}', 'LokasiController@edit');
Route::post('/lokasi/store', 'LokasiController@store');
Route::post('/lokasi/update', 'LokasiController@update');
Route::post('/lokasi/hapus', 'LokasiController@hapus');

Route::get('/pergerakan', 'PergerakanController@index')->name('index_pergerakan');
Route::get('/pergerakan/detail/{id}', 'PergerakanController@detail');
Route::get('/pergerakan/create', 'PergerakanController@create');
Route::get('/pergerakan/status/{id}/{status}/{lokasi}', 'PergerakanController@status');
Route::get('/pergerakan/updatestatus/{id}', 'PergerakanController@updatestatus');
Route::post('/pergerakan/store', 'PergerakanController@store');
Route::get('/pergerakan/qrcode/{id}', 'PergerakanController@qrcode');
Route::get('/pergerakan/cetak_qrcode/{id}', 'PergerakanController@cetak_qrcode');

Route::get('/barang_masuk', 'BarangMasukController@index')->name('index_barang_masuk');
Route::get('/barang_masuk/create', 'BarangMasukController@create');
Route::get('/barang_masuk/edit/{id}', 'BarangMasukController@edit');
Route::post('/barang_masuk/store', 'BarangMasukController@store');
Route::post('/barang_masuk/update', 'BarangMasukController@update');
Route::post('/barang_masuk/hapus', 'BarangMasukController@hapus');

Route::get('/detailbarang/{id}', 'DetailBarangController@index');