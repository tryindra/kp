<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryBarang extends Model
{
    //
    protected $table = 'history_barang';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
