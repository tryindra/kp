<?php

namespace App\Http\Controllers;

use App\Barang;
use App\Kategori;
use Illuminate\Http\Request;
use DB;

class BarangController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //        
        // $ret = Barang::where('hapuskah', 0)->where('status', 1)->get();
        $ret = DB::table('barang')
            ->select('barang.*', 'kategori.nama_kategori')
            ->join('kategori', 'kategori.id', '=', 'barang.kategori_id')->where('barang.hapuskah', 0)->where('kategori.hapuskah', 0)->get();
        $data = array('data' => $ret);
        return view('barang/index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ret = Kategori::where('hapuskah', 0)->get();
        $data = array('data' => $ret);
        return view('barang/create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $request->validate(
            [
                'nama_barang' => 'required|string|max:255|unique:barang,nama_barang',
                'kategori' => 'required',
            ],
            [
                'nama_barang.required' => 'Silahkan Isi Nama Barang Terlebih Dahulu.',
                'nama_barang.unique' => 'Nama Barang Telah Tercatat. Silahkan Masukkan Nama Barang Lainnya.',
                'nama_barang.max' => 'Maksimal Terdiri Dari 255 Karakter.',
                'nama_barang.string' => 'Inputan Harus Berupa Teks Huruf/Angka',
                'kategori.required' => 'Silahkan Pilih Kategori Terlebih Dahulu.',
            ]);
    
            $barang = new Barang;
            $barang->nama_barang = $request->nama_barang;
            $barang->kategori_id = $request->kategori;
            $barang->deskripsi = $request->deskripsi;
            $barang->barcode = "";
            $barang->gambar_barang = "";
            $barang->hapuskah = false;

            $barang->status = false;
            if($request->hapuskah)
            {
                $barang->status = true;
            }
            $barang->save();
    
            return redirect()->route('index_barang');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function hapus(Request $request)
    {
        $validatedData = $request->validate(
        [
            'id' => 'required',
        ],
        [
            'id.required' => 'ID Tidak Ditemukan',
        ]);

        $barang = Barang::find($request->id);
        $barang->hapuskah = true;
        $update = $barang->update();

        if($update)
        {
            return json_encode(array('status' => true));
        }
        else
        {
            return json_encode(array('status' => false, 'pesan' => 'Data Gagal Dihapus! Silahkan Coba Lagi.'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $ret = Barang::find($id);

        if($ret)
        {
            $ret = DB::table('barang')
                    ->select('barang.*')
                    ->join('kategori', 'kategori.id', '=', 'barang.kategori_id')
                    ->where('barang.id', $id)
                    ->where('barang.hapuskah', 0)
                    ->where('kategori.hapuskah', 0)
                    ->get();

            $data_kategori = Kategori::where('hapuskah', 0)->get();
            
            return view('barang/edit', ['data' => $ret, 'data_kategori' => $data_kategori]);
        }
        else
        {
            return redirect()->route('index_barang');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $validatedData = $request->validate(
            [
                'id' => 'required|numeric',
                'nama_barang' => 'required|string|max:255',
                'kategori' => 'required|numeric',
            ],
            [
                'id.required' => 'ID Tidak Ditemukan',
                'id.numeric' => 'ID Tidak Ditemukan',

                'nama_barang.required' => 'Silahkan Isi Nama Barang Terlebih Dahulu.',
                'nama_barang.max' => 'Nama Barang Maksimal Terdiri Dari 255 Karakter.',
                'nama_barang.string' => 'Nama Barang Harus Berupa Teks Huruf/Angka',
    
                'kategori.required' => 'Kategori Tidak Ditemukan',
                'kategori.numeric' => 'Kategori Tidak Ditemukan',
            ]);
            
            $cek = Barang::where('nama_barang', $request->nama_barang)->where('id', '<>', $request->id)->count();
            if($cek)
            {
                return redirect('/barang/edit/'.$request->id)->withErrors(['nama_barang' => 'Nama Barang Telah Tercatat. Silahkan Masukkan Lokasi Lainnya.']); 
            }
            else
            {
                $barang = Barang::find($request->id);
                $barang->nama_barang = $request->nama_barang;
                $barang->kategori_id = $request->kategori;
                $barang->deskripsi = $request->deskripsi;
    
                $barang->status = false;
                if($request->status)
                {
                    $barang->status = true;
                }
                $barang->update();
    
                return redirect()->route('index_barang');
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
