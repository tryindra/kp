@extends('layouts.index')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">DETAIL PERGERAKAN BARANG</h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
		<div class="row mb-2">
		</div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
                    <div class="col-md-6">
                        <div class="card card-primary card-outline">

                            <div class="card-header">
                                <h4>Data Barang {{ $data[0]->nama_status }}</h4>
                            </div>

                            <div class="card-body">

                                <div class="row justify-content-sm-center">
                                    <div class="col-sm-4 justify-content-sm-end text-right">
                                        <div>
                                            <label>ID:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div>
                                            <span>{{ $data[0]->id }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-sm-center">
                                    <div class="col-sm-4 justify-content-sm-end text-right">
                                        <div>
                                            <label>Status:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div>
                                            <span>{{ $data[0]->nama_status }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-sm-center">
                                    <div class="col-sm-4 justify-content-sm-end text-right">
                                        <div>
                                            <label>Tanggal:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div>
                                            <span>{{ $data[0]->tanggal }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-sm-center">
                                    <div class="col-sm-4 justify-content-sm-end text-right">
                                        <div>
                                            <label>Lokasi Barang:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div>
                                            <span>{{ $data[0]->nama_lokasi }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-sm-center">
                                    <div class="col-sm-4 justify-content-sm-end text-right">
                                        <div>
                                            <label>Catatan:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div>
                                            <span>{{ $data[0]->catatan == "" ? "-" : $data[0]->catatan }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-sm-center">
                                    <div class="col-sm-4 justify-content-sm-end text-right">
                                        <div>
                                            <label>QRCode:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div>
                                            <button onclick="qrcode({{ $data[0]->id }})" style="padding: 0; vertical-align: top; font-size: 15px;" class="btn btn-sm btn-link">Klik Untuk Melihat QRCode</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h4>Data Barang Masuk</h4>
                            </div>
                            <div class="card-body">

                                <div class="row justify-content-sm-center">
                                    <div class="col-sm-4 justify-content-sm-end text-right">
                                        <div>
                                            <label>Nama Barang:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div>
                                            <span>{{ $data[0]->nama_barang }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-sm-center">
                                    <div class="col-sm-4 justify-content-sm-end text-right">
                                        <div>
                                            <label>Tanggal Barang Masuk:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div>
                                            <span>{{ $data[0]->tanggal_barang_masuk }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-sm-center">
                                    <div class="col-sm-4 justify-content-sm-end text-right">
                                        <div>
                                            <label>Jumlah Barang Masuk:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div>
                                            <span>{{ $data[0]->jumlah_barang_masuk }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-sm-center">
                                    <div class="col-sm-4 justify-content-sm-end text-right">
                                        <div>
                                            <label>Lokasi Awal:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div>
                                            <span>{{ $lokasi_awal[0]->nama_lokasi }}</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

				<div class="row">
                    <div class="col-md-6">
                        <div class="card card-primary card-outline">

                            <div class="card-header">
                                <h4>History Barang</h4>
                            </div>

                            <div class="card-body">

                                <div class="timeline">

                                    @php
                                    $temp_tanggal = "";   
                                    @endphp
                                    @foreach ($data_history as $key=>$history)
                                        @if($temp_tanggal != date('d M Y', strtotime($history->tanggal)))                                      
                                            @php
                                                $temp_tanggal = date('d M Y', strtotime($history->tanggal));   
                                            @endphp

                                    <div class="time-label">
                                        <span class="bg-info">{{ $temp_tanggal }}</span>
                                    </div>

                                        @endif
                                    <div>
                                        <i class="fas fa-pallet bg-info"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fas fa-clock"></i> {{ date('H:i', strtotime($history->tanggal)) }}</span>
                                            <h3 class="timeline-header"><b>{{ ($key+1) == count($data_history) ? "Barang Baru Masuk" : "Update Status Barang" }}</b></h3>
                        
                                            <div class="timeline-body">
                                                Barang <b>{{ $history->status }}</b>
                                                <br>
                                                Lokasi Barang Di <b>{{ $history->nama_lokasi}}</b>
                                                <br><br>

                                                <b>Catatan:</b> <br>
                                                {{ $history->catatan != "" ? $history->catatan : '-' }}
                                            </div>

                                            <div class="timeline-footer" style="font-size: 13px; color: grey;">
                                                Oleh User:
                                            </div>
                                        </div>
                                    </div>
                                        
                                    @endforeach

                                    <div>
                                        <i class="fas fa-stop-circle bg-gray"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h4>Update Status</h4>
                            </div>
                            <form role="form" method="POST" action="{{url('pergerakan/store')}}" autocomplete="off">
                                @csrf
                                
                                <input type="hidden" name="id" value="{{ $data[0]->id }}">

                                <div class="card-body">    

                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <strong>PERHATIAN!</strong> Terdapat Error! Silahkan Periksa Kembali Inputan Anda!.
                                            <br/>
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <div class="row justify-content-md-center">
                                        <div class="col-md-12">
                                            <div class="form-group @error('status') has-error @enderror">
                                                <label for="status">Status:</label>
            
                                                <select id="status" name="status" class="form-control">
                                                    <option value="" disabled selected>-- PILIH STATUS --</option>
            
                                                @foreach ($data_status as $data_statuss)
                                                        
                                                    <option value="{{ $data_statuss->id }}">{{ $data_statuss->status }}</option>
                                                
                                                @endforeach
            
                                                </select>
                                                
                                                @error('status')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
						
                                    <div class="row justify-content-md-center">
                                        <div class="col-md-12">
                                            <div class="form-group @error('alamat') has-error @enderror">
                                                <label for="alamat">Lokasi:</label>
            
                                                <select id="alamat" name="alamat" class="form-control">
                                                    <option value="" disabled selected>-- PILIH LOKASI --</option>
            
                                                @foreach ($data_lokasi as $data_lokasis)
                                                    
                                                    <option value="{{ $data_lokasis->id }}">{{ $data_lokasis->nama_lokasi }}</option>
                                                
                                                @endforeach
            
                                                </select>
            
                                                @error('alamat')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row justify-content-md-center">
                                        <div class="col-md-12">
                                            <div class="form-group @error('tanggal') has-error @enderror">
                                                <label for="tanggal">Tanggal:</label>
                                                
                                                <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                                    <input type="text" class="form-control datetimepicker-input" id="datepicker" name="tanggal"/>
                                                </div>
            
                                                @error('tanggal')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row justify-content-md-center">
                                        <div class="col-md-12">
                                            <div class="form-group @error('catatan') has-error @enderror">
                                                <label for="catatan">Catatan:</label>
                                                
                                                <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                                    <textarea style="resize: none; height: 100px;" class="form-control" name="catatan" placeholder="Contoh: Bagian Barang Yang Rusak & Perlu Diperbaiki Adalah Bagian Depan & Samping"></textarea>
                                                </div>
            
                                                @error('catatan')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row justify-content-md-center">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-info">Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>
  <!-- /.content -->
@endsection
