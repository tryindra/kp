<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pergerakan extends Model
{
    //
    protected $table = 'pergerakan_barang';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
